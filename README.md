# Crypto Currency Chart

Uses SocketIO and danielgindi/Charts to show a candle stick chart of BTC/JPY trades.

Each entry in the chart represents one minute. Please launch the app and leave it open until the chart updates with a few mintues of data.

## How to use

1. Clone this repo.
2. `$ carthage update --platform iOS`.
3. Open `CryptoChart.xcodeproj`.
4. Run.

## Still to do
* Add a dropdown to show different currency combinations.
* Refactor code out of the view controller.
* Nicely format the chart.
