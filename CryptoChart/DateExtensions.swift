import Foundation

extension Date {
    
    // Zeroes out the seconds
    var currentMinute: Date {
        get {
            let calender = Calendar.init(identifier: .gregorian)
            let components = calender.dateComponents([.year, .month, .day, .hour, .minute], from: self)
            return calender.date(from: components)!
        }
    }
    
}
