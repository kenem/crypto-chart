//
//  AppDelegate.swift
//  CryptoChart
//
//  Created by Ken Myers on 2019/01/29.
//  Copyright © 2019 Ken Myers. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
}

