//
//  ViewController.swift
//  CryptoChart
//
//  Created by Ken Myers on 2019/01/29.
//  Copyright © 2019 Ken Myers. All rights reserved.
//

import UIKit
import SocketIO
import Charts

class ViewController: UIViewController {

    var entries: Dictionary = [Date: CandleChartDataEntry]()
    
    var chartView: CandleStickChartView = CandleStickChartView()
    
    var count: Double = 0
    
    let socketManager: SocketManager = SocketManager(socketURL:
        URL(string: "https://io.lightstream.bitflyer.com")!,
            config: [.log(true), .forceWebsockets(true)])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var rect = view.bounds
        rect.origin.y += 20
        rect.size.height -= 20
        chartView.frame = rect
        self.view.addSubview(chartView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let channelName = "lightning_executions_BTC_JPY"
        
        let socket = socketManager.defaultSocket
        
        socket.on(clientEvent: .connect) { [weak socket] _, _ in
            socket?.emit("subscribe", channelName)
        }
        
        socket.on(channelName) { messages, _  in
            print(messages)
            dump(messages)
            
            self.updateChartWith(messages: messages)
        }
        socket.connect()
        
        
    }
    
    func updateChartWith(messages: Array<Any>) {
        // Todo: need to refactor and clean up
        let response = messages[0] as AnyObject?
        let trades = (response as! NSArray) as Array
        for trade in trades {
            let tradePrice: Double = trade["price"] as! Double
            let entry = self.entries[Date().currentMinute]
            if entry != nil {
                // Second trade or later this minute
                if tradePrice > entry!.high {
                    entry!.high = tradePrice
                }
                if tradePrice < entry!.low {
                    entry!.low = tradePrice
                }
                entry!.close = tradePrice
            } else {
                // First trade of this minute
                self.entries[Date().currentMinute] = CandleChartDataEntry(x: self.count,
                                                                          shadowH: tradePrice,
                                                                          shadowL: tradePrice,
                                                                          open: tradePrice,
                                                                          close: tradePrice)
                self.count += 1
            }
        }
        let set = CandleChartDataSet(values: Array(self.entries.values), label: "Test")
        self.chartView.data = CandleChartData(dataSet: set)
    }
}

